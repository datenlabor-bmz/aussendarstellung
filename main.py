from dash import Dash, html, dcc, Input, Output, dash_table, State
import dash_bootstrap_components as dbc
from dash import Dash, html, dcc, Input, Output, State
import plotly.express as px
import pandas as pd

dat = pd.read_excel("https://github.com/datenlabor01/aussendarstellung/raw/main/testdata_processed.xlsx")
app = Dash(external_stylesheets = [dbc.themes.ZEPHYR])

dat["Standzeit bis"] = dat["Aktuell besetzt bis"].str[:4]

region_dropdown = dcc.Dropdown(id = "region", options=sorted(dat["Region"].unique()),
                             value='All', style={"textAlign": "center"}, clearable=False, multi=True, placeholder='Alle Regionen')

time_dropdown = dcc.Dropdown(id = "time", options=sorted(dat["Standzeit bis"].unique()),
                             value='All', style={"textAlign": "center"}, clearable=False, multi=True, placeholder='Alle Standzeiten')

dotation_dropdown = dcc.Dropdown(id = "dotation", options=sorted(dat["Maximale Dotierung"].unique()),
                             value='All', style={"textAlign": "center"}, clearable=False, multi=True, placeholder='Alle Dotierungen')


app.layout = html.Div([
      dbc.Row([
         html.H1(children='Prototyp Außendarstellung', style={'textAlign': 'center'}),
         html.P(children = "Das ist ein Prototyp.",
         style={'textAlign': 'center'}),
      ]),

      dbc.Row([
          dcc.Graph("map", style={'textAlign': 'center'})
      ]),

      dbc.Row([html.Br(),
        dbc.Col([region_dropdown]),
        dbc.Col([time_dropdown]),
        dbc.Col([dotation_dropdown]), html.Br(),
    ], style={'textAlign': 'center'}),

      #Data Table:
      dbc.Row([
         my_table := dash_table.DataTable(
         dat.to_dict('records'), [{"name": i, "id": i} for i in dat.columns[:-5]],
         filter_action="native", sort_action="native", page_size= 25,
         style_cell={'textAlign': 'left', "whiteSpace": "normal", "height": "auto"},
         style_header={'backgroundColor': 'rgb(11, 148, 153)', 'color': 'black', 'fontWeight': 'bold'},
             style_data_conditional=[{
            'if': {'row_index': 'odd'},
            'backgroundColor': 'rgb(235, 240, 240)',
        }]),
         ]),
])

#Callback for dropdown:
@app.callback(
    [Output('time', 'options'), Output('dotation', 'options')],
    [Input("region", 'value')]
)

def update_dropdown(region):
  if (region == "All") | (region == []):
      dat_fil = dat
  else:
      dat_fil = dat[dat["Region"].isin(region)]
  return sorted(dat_fil["Standzeit bis"].unique()), sorted(dat["Maximale Dotierung"].unique())

@app.callback(
    Output('region', 'options'),
    [Input("time", 'value'), Input("dotation", 'value')]
)

def update_dropdown2(time, dotation):
  if (time == "All") | (time == []):
      dat_fil = dat
  else:
      dat_fil = dat[dat["Standzeit bis"].isin(time)]
  if (dotation == "All") | (dotation == []):
      dat_fil = dat_fil
  else:
      dat_fil = dat_fil[dat_fil["Maximale Dotierung"].isin(dotation)]

  return sorted(dat_fil["Region"].unique())

@app.callback(
    [Output(my_table, "data"), Output("map", "figure")],
    [Input("region", 'value'), Input("time", 'value'), Input("dotation", 'value')]
)

def update_table(region, time, dotation):

   if (region == "All") | (region == []):
      dat_fil = dat
   else:
      dat_fil = dat[dat["Region"].isin(region)]
   if (time == "All") | (time == []):
      dat_fil = dat_fil
   else:
      dat_fil = dat_fil[dat_fil["Standzeit bis"].isin(time)]
   if (dotation == "All") | (dotation == []):
      dat_fil = dat_fil
   else:
      dat_fil = dat_fil[dat_fil["Maximale Dotierung"].isin(dotation)]

   figMap = px.scatter_mapbox(dat_fil[(dat_fil.Lat!= "")& (dat_fil.Lat != "not found")], lat="Lat", lon="Lon",
                        hover_name="Ort", hover_data={"Name": True, "Besetzt seit": True, "Aktuell besetzt bis": True, "Ort": False, "Land ": False,
                        "Maximale Dotierung": True, "Lat": False, "Lon": False},
                        zoom = 2)
   figMap.update_layout(mapbox_style="carto-positron")
   figMap.update_layout(margin={"r":0,"t":0,"l":0,"b":0})

   return dat_fil.to_dict("records"), figMap

if __name__ == '__main__':
    app.run_server(debug=True)
